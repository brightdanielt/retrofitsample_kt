最近學了一點 kotlin 的基本，決定寫個 Retrofit2 呼叫 TMdbApi
熟悉一下 kotlin～

另外，使用Coroutine
    請參考：[Coroutines on Android]<br>
    以及萊姐(Lyla)的文章：[ViewModels with Saved State, Jetpack Navigation, Data Binding and Coroutines]

2019/6/21<br>
    為了更好的 UX 及節省網路用量，為搜尋 TMDB 資源加入快取(Cache)機制，what? 不懂快取?
    沒關係，去爬個文<br>
    [Cache原理] <br>
    [Android實作]<br>
    [Android實作 + coroutine epam版]<br>
    [Android實作 + coroutine diefferson版]
    
2019/6/25
    自訂快取目錄，結構如下圖
    <img src="app/tmdb_cache_dir.png" >
    
2019/6/30
    處理呼叫 api 可能發生的 exception。
    行動裝置網路未連接時，也記得提示使用者相關訊息
    參考這篇：[Advanced Retrofit2 (Part 1): Network Error Handling & Response Caching]
    
[Advanced Retrofit2 (Part 1): Network Error Handling & Response Caching]:https://medium.com/@tsaha.cse/advanced-retrofit2-part-1-network-error-handling-response-caching-77483cf68620
[ViewModels with Saved State, Jetpack Navigation, Data Binding and Coroutines]:https://medium.com/androiddevelopers/viewmodels-with-saved-state-jetpack-navigation-data-binding-and-coroutines-df476b78144e
[Coroutines on Android]:https://medium.com/androiddevelopers/coroutines-on-android-part-i-getting-the-background-3e0e54d20bb
[Cache原理]: https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/http-caching
[Android實作]:https://medium.com/@I_Love_Coding/how-does-okhttp-cache-works-851d37dd29cd
[Android實作 + coroutine epam版]:https://proandroiddev.com/caching-with-kotlin-coroutines-7d819276c820
[Android實作 + coroutine diefferson版]:https://medium.com/@dieffersonsantos/simple-android-cache-powered-by-coroutines-42db61306569
