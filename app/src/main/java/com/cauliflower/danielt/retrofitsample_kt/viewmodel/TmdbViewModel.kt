package com.cauliflower.danielt.retrofitsample_kt.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.cauliflower.danielt.retrofitsample_kt.App
import com.cauliflower.danielt.retrofitsample_kt.MainActivity
import com.cauliflower.danielt.retrofitsample_kt.data.EpamRepository
import com.cauliflower.danielt.retrofitsample_kt.data.MovieItem
import kotlinx.coroutines.launch

class TmdbViewModel constructor(application: Application) : androidx.lifecycle.AndroidViewModel(application) {
    private val app = application as App
    private val repository = app.movieRepository
    private val dieffersonRepository = app.dieffersonRepository

    var mutableMovieItem = MutableLiveData<MovieItem?>()

    fun getMovieByIdTest(movieId: String) {

        //Epam cache 測試中
        viewModelScope.launch {
            val repository = EpamRepository(
                app.tmdbCacheDir,
                app.apiWorker.tmdbApiService
            )
            val movieItem = repository.getMovieById(movieId).await()
            mutableMovieItem.postValue(movieItem)
        }
    }

    fun getMovieByIdOk(movieId: String) {
        viewModelScope.launch {
            val movieItem = repository.getMovieByIdOk(movieId)
            //Diefferson cache 可行
//            val movieItem = dieffersonRepository.getMovieByIdOk(movieId)
            mutableMovieItem.postValue(movieItem)
        }
    }

    fun deleteDieffersonCache(movieId: String = "") {
        dieffersonRepository.deleteCache(movieId)
    }

    class Factory(private val application: Application) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(MainActivity::class.java)) {
                return TmdbViewModel(application) as T
            } else {
                throw UnsupportedOperationException("unknown class:{${modelClass.simpleName}}")
            }
        }
    }
}