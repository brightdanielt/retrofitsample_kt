package com.cauliflower.danielt.retrofitsample_kt.data

import android.util.Log
import com.cauliflower.danielt.retrofitsample_kt.App
import com.cauliflower.danielt.retrofitsample_kt.SingletonHolder
import com.cauliflower.danielt.retrofitsample_kt.cache.CacheProviders
import com.cauliflower.danielt.retrofitsample_kt.network.TmdbApiService
import com.epam.coroutinecache.api.CacheParams
import com.epam.coroutinecache.api.CoroutinesCache
import com.epam.coroutinecache.mappers.GsonMapper
import io.coroutines.cache.core.CachePolicy
import kotlinx.coroutines.Deferred
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.util.concurrent.TimeUnit

//這是部門功能的基礎規範
open class BaseRepository {

    /*主管*/
    //主管也需要知道 如何辦這件事 以及 任務失敗的指定訊息，再交予業務處理
    //主管得到業務給的禮盒後，直接根據禮盒的顏色分辨事件的成功與否，
    //並執行相對應的動作，
    // 失敗：記錄失敗檔案，成功：上交禮盒
    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>, errorMessage: String): T? {
        val result = safeApiResult(call, errorMessage)
        var data: T? = null
        when (result) {
            is Result.Success -> {
                data = result.data
            }
            is Result.Error -> {
                Log.e(this::class.java.simpleName, result.exception.message)
            }
        }
        return data
    }

    /*業務*/
    //業務被主管委託辦一件事，而主管必須告知業務如何辦這件事，且辦完這件事後，業務會得到某類型的包裹，
    //而且業務需要包裝該物品，該物品的包裝也有規定，事情辦得成功，只需把該包裹拆開，將物品裝入白色禮盒，
    //若辦得失敗，則黑色禮盒中將裝入主管的指定訊息以及失敗的原因，最後將禮盒交予主管
    private suspend fun <T : Any> safeApiResult(call: suspend () -> Response<T>, errorMessage: String): Result<T> {
        val response: Response<T>
        try {
            response = call.invoke()
        } catch (e: java.lang.Exception) {
            return Result.Error(Exception("Exception occurred during getting safe Api result, message:${e.message}"))
        }
        if (response.isSuccessful) return Result.Success(response.body()!!)
        //哈哈 這邊的設計怪怪的，error應該是從源頭往外傳ＸＤＤＤ
        return Result.Error(IOException("Error occurred during getting safe Api result, Custom ERROR - $errorMessage"))
    }
}

/*Movie 部門*/
class MovieRepository constructor(private val api: TmdbApiService) :
    BaseRepository() {

    //這裡是受理 MovieItem 業務的窗口，且明確指出如何執行該任務
    suspend fun getMovieByIdOk(movieId: String): MovieItem? {
        return safeApiCall(
            call = { api.getMovieById2(movieId) },
            errorMessage = "Error fetch movie:{$movieId}"
        )
    }
}

class EpamRepository(cacheDirectory: File, private val api: TmdbApiService) {
    val coroutineCache = CoroutinesCache(CacheParams(10, GsonMapper(), cacheDirectory))
    val cacheProviders = coroutineCache.using<CacheProviders>(CacheProviders::class.java)

    suspend fun getMovieById(movieId: String): Deferred<MovieItem> {
        return cacheProviders.getMovieItem({ api.getMovieById4(movieId) })
    }
}

class DieffersonRepository private constructor(app: App) :
    io.coroutines.cache.core.CoroutinesCache(app) {

    companion object : SingletonHolder<DieffersonRepository, App>(::DieffersonRepository)

    private val api: TmdbApiService = app.apiWorker.tmdbApiService
    suspend fun getMovieByIdOk(movieId: String): MovieItem {
        return asyncCache(
            { api.getMovieById4(movieId) },
            "MovieItem_$movieId",
            CachePolicy.TimeCache(12, TimeUnit.HOURS)
        ).await()
    }

    //The format of key is MovieItem_$movieId
    fun deleteCache(movieId: String = "") {
        if (movieId.isEmpty()) {
            clear()
        } else {
            deleteItem("MovieItem_$movieId")
        }
    }
}
