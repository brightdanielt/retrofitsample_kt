package com.cauliflower.danielt.retrofitsample_kt

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.cauliflower.danielt.retrofitsample_kt.cache.CacheUtils
import com.cauliflower.danielt.retrofitsample_kt.data.MovieItem
import com.cauliflower.danielt.retrofitsample_kt.network.InternetConnectionListener
import com.cauliflower.danielt.retrofitsample_kt.viewmodel.TmdbViewModel
import com.cauliflower.danielt.retrofitsample_kt.viewmodel.getViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity(), InternetConnectionListener {
    private var disposable: Disposable? = null

    lateinit var app: App

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        app = application as App
        app.internetConnectionListener = this
//        getMovieById()

        getMovieByIdFromViewModel()
    }

    override fun onInternetUnavailable() {
        runOnUiThread {
            Toast.makeText(this, "Internet unavailable", Toast.LENGTH_SHORT).show()
            Log.i(this.localClassName, "Internet unavailable")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        app.internetConnectionListener = null
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
//            R.id.action_delete_cache -> deleteDieffersonCache()
            R.id.action_delete_cache -> deleteAppCache()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onStop() {
        super.onStop()
        if (disposable != null) {
            disposable!!.dispose()
        }
    }

    private fun getMovieByIdFromViewModel() {
// Old option
        /*val viewModel: () -> TmdbViewModel = {
            val factory = TmdbViewModel.Factory(application)
            ViewModelProviders.of(this, factory).get(TmdbViewModel::class.java)
        }
// New option
        val viewModel2: () -> TmdbViewModel = {
            val factory = BaseViewModelFactory<TmdbViewModel> {
                TmdbViewModel(application)
            }
            ViewModelProviders.of(this, factory).get(TmdbViewModel::class.java)
        }
        val viewModel3: TmdbViewModel = getViewModel { TmdbViewModel(application) }*/

        val viewModel4 = getViewModel<TmdbViewModel>()

        viewModel4.getMovieByIdOk("76341")
        viewModel4.mutableMovieItem.observe(this, Observer { movieItem ->
            if (movieItem != null) {
                toastMovieInfo(movieItem)
            }
        })
    }

    private fun getMovieById() {
        disposable = app.apiWorker.tmdbApiService.getMovieById("76341")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { movieItem -> toastMovieInfo(movieItem) },
                { error -> toastError(error) })
    }

    private fun toastError(error: Throwable) = Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show()

    private fun toastMovieInfo(movieItem: MovieItem) =
        Toast.makeText(this, movieItem.original_title, Toast.LENGTH_SHORT).show()

    private fun deleteDieffersonCache(movieId: String = "") {
        app.dieffersonRepository.deleteCache(movieId)
    }

    private fun deleteAppCache() {
        CacheUtils.deleteCache(app)
    }
}
