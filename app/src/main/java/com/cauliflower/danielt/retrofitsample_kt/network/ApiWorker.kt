package com.cauliflower.danielt.retrofitsample_kt.network

import com.cauliflower.danielt.retrofitsample_kt.App
import com.cauliflower.danielt.retrofitsample_kt.BuildConfig
import com.cauliflower.danielt.retrofitsample_kt.SingletonHolder
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private var mClient: OkHttpClient? = null
private var mGsonConverterFactory: GsonConverterFactory? = null
private var mRetrofit: Retrofit? = null
private var mTmdbApiService: TmdbApiService? = null
private var tmdbCache: Cache? = null

class ApiWorker private constructor(val app: App) {

    companion object : SingletonHolder<ApiWorker, App>(::ApiWorker)

    val networkConnectionInterceptor = object : NetworkConnectionInterceptor() {
        override fun isNetworkAvailable(): Boolean {
            return app.isNetworkAvailable()
        }

        override fun onNetworkUnavailable() {
            app.internetConnectionListener?.onInternetUnavailable()
        }

    }

    // Find the corresponding cache even there is no internet
    val forceCacheInterceptor = Interceptor { chain ->
        val newBuilder = chain.request().newBuilder()
        newBuilder.cacheControl(CacheControl.FORCE_CACHE)
        return@Interceptor chain.proceed(newBuilder.build())
    }

    //To help you add api_key in every request
    val requestInterceptor = Interceptor { chain ->
        val newUrl = chain.request().url()
            .newBuilder()
            .addQueryParameter("api_key", BuildConfig.TMDB_API_KEY)
            .build()

        val newRequest = chain.request()
            .newBuilder()
            .url(newUrl)
            .build()
        chain.proceed(newRequest)
    }

    val client: OkHttpClient
        get() {
            if (mClient == null) {
                val loggingInterceptor = HttpLoggingInterceptor()
                loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

                tmdbCache = Cache(app.tmdbCacheDir, 10 * 1024 * 1024)

                mClient = OkHttpClient.Builder()
                    .cache(tmdbCache)
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS)
//                .writeTimeout()
                    //To show json of request and response in logcat
                    .addInterceptor(loggingInterceptor)
                    .addInterceptor(requestInterceptor)
//                    .addInterceptor(forceCacheInterceptor)
                    .addInterceptor(networkConnectionInterceptor)
                    .build()
            }
            return mClient!!
        }

    val gsonConverterFactory: GsonConverterFactory
        get() {
            if (mGsonConverterFactory == null) {
                mGsonConverterFactory = GsonConverterFactory.create(
                    GsonBuilder()
                        .setLenient()
                        .disableHtmlEscaping()
                        .create()
                )
            }
            return mGsonConverterFactory!!
        }

    val retrofit: Retrofit
        get() {
            if (mRetrofit == null) {
                mRetrofit = Retrofit.Builder()
                    .baseUrl(BuildConfig.TMDB_BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(gsonConverterFactory)
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .client(client)
                    .build()
            }
            return mRetrofit!!
        }

    val tmdbApiService: TmdbApiService
        get() {
            if (mTmdbApiService == null) {
                mTmdbApiService = retrofit.create(
                    TmdbApiService::class.java
                )
            }
            return mTmdbApiService!!
        }
}