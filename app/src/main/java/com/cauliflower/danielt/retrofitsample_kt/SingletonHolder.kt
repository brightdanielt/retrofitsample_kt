package com.cauliflower.danielt.retrofitsample_kt
/*
* Post about singleton:
* https://kotlinlang.org/docs/tutorials/kotlin-for-py/objects-and-companion-objects.html
* https://medium.com/@BladeCoder/kotlin-singletons-with-argument-194ef06edd9e
* https://juejin.im/post/5acf49a06fb9a028d700ff4d
* */
open class SingletonHolder<out T, in A>(private val constructor: (A) -> T) {
    @Volatile
    private var instance: T? = null

    private fun createInstance(arg: A): T {
        return synchronized(this) {
            instance ?: constructor(arg).also { instance = it }
        }
    }

    //Remember to give me arg at first time or you got exception
    fun getInstance(arg: A?=null): T {
        return when {
            instance == null && arg != null -> createInstance(arg)
            else -> instance ?: throw ExceptionInInitializerError(
                "Forgot to give arg at first time you call getInstance")
        }
    }

}
