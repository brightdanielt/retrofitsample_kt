package com.cauliflower.danielt.retrofitsample_kt

import android.app.Application
import android.net.ConnectivityManager
import com.cauliflower.danielt.retrofitsample_kt.cache.CacheUtils
import com.cauliflower.danielt.retrofitsample_kt.data.DieffersonRepository
import com.cauliflower.danielt.retrofitsample_kt.data.MovieRepository
import com.cauliflower.danielt.retrofitsample_kt.network.ApiWorker
import com.cauliflower.danielt.retrofitsample_kt.network.InternetConnectionListener
import java.io.File

class App : Application() {

    lateinit var tmdbCacheDir: File

    lateinit var apiWorker: ApiWorker

    lateinit var movieRepository: MovieRepository

    lateinit var dieffersonRepository: DieffersonRepository

    var internetConnectionListener: InternetConnectionListener? = null

    override fun onCreate() {
        super.onCreate()
        tmdbCacheDir = CacheUtils.createCacheChildDir(this, "TMDB")
        apiWorker = ApiWorker.getInstance(this)
        movieRepository = MovieRepository(apiWorker.tmdbApiService)
        dieffersonRepository = DieffersonRepository.getInstance(this)
    }

    fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(ConnectivityManager::class.java)
        return connectivityManager.activeNetwork != null && connectivityManager.activeNetworkInfo.isConnected
    }
}
