package com.cauliflower.danielt.retrofitsample_kt.network

import com.cauliflower.danielt.retrofitsample_kt.data.MovieItem
import io.reactivex.Observable
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface TmdbApiService {

    @GET("movie/{movieId}")
    fun getMovieById(@Path("movieId") movieId: String): Observable<MovieItem>

    //https://github.com/JakeWharton/SdkSearch/pull/170/commits/afdb29405500fc15882017ad9a3028b5df16c7ea
    @GET("movie/{movieId}")
    suspend fun getMovieById2(@Path("movieId") movieId: String): Response<MovieItem>


    @GET("movie/{movieId}")
    suspend fun getMovieById3(@Path("movieId") movieId: String): MovieItem

    @GET("movie/{movieId}")
    fun getMovieById4(@Path("movieId") movieId: String): Deferred<MovieItem>

}