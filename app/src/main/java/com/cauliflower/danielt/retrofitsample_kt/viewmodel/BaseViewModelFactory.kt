package com.cauliflower.danielt.retrofitsample_kt.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * High level function helps us get ViewModel more accurate.
 * Ex: To get TargetViewModel calling code bellow
 *
 * val factory = BaseViewModelFactory(TargetViewModel(param1,param2))
 * val targetViewModel = ViewModelProviders.of(activity, factory).get(TargetViewModel::class.java)
 * */
class BaseViewModelFactory<T>(val creator: () -> T) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return creator() as T
    }
}