package com.cauliflower.danielt.retrofitsample_kt.cache

import android.content.Context
import android.util.Log
import com.cauliflower.danielt.retrofitsample_kt.App
import java.io.File

object CacheUtils {
    fun deleteCache(app: App): Boolean {
        return deleteDir(app.tmdbCacheDir)
    }

    private fun deleteDir(dir: File?): Boolean {
        when {
            dir != null && dir.isDirectory -> {
                var success: Boolean
                dir.listFiles().forEach { success = deleteDir(it);if (!success) return@deleteDir success }
                return dir.delete()
            }
            dir != null && dir.isFile -> return dir.delete()
            else -> return false
        }
    }

    fun createCacheChildDir(context: Context, child: String): File {
        val file = File(context.cacheDir, child)
        if (!file.exists()) {
            file.mkdir()
        }
        return file.also {
            Log.i(this::class.java.simpleName, "createCacheChildDir:${it.absolutePath}")
        }
    }
}

