package com.cauliflower.danielt.retrofitsample_kt.cache

import com.cauliflower.danielt.retrofitsample_kt.data.MovieItem
import com.epam.coroutinecache.annotations.Expirable
import com.epam.coroutinecache.annotations.LifeTime
import com.epam.coroutinecache.annotations.ProviderKey
import com.epam.coroutinecache.annotations.UseIfExpired
import kotlinx.coroutines.Deferred
import java.util.concurrent.TimeUnit

//For com.epam.coroutinecache:coroutinecache
interface CacheProviders {
    @ProviderKey("TestKey")
    @LifeTime(3600 * 12, TimeUnit.SECONDS)
    @Expirable
    //小心使用，即使該快取資料的時效到了，仍然會使用該快取資料，但只使用一次
    @UseIfExpired
    suspend fun getMovieItem(dataProvider: suspend () -> Deferred<MovieItem>): Deferred<MovieItem>
}