package com.cauliflower.danielt.retrofitsample_kt.network

interface InternetConnectionListener {
    fun onInternetUnavailable()
}